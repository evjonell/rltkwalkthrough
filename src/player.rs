use std::cmp::{max, min};

use bevy::prelude::*;

use crate::{
    components::{CombatStats, WantsToMelee},
    Map, Player, Position, RunState, State, Viewshed,
};

pub struct PlayerPlugin;

impl Plugin for PlayerPlugin {
    fn build(&self, app: &mut App) {
        app.add_system_set(SystemSet::on_update(RunState::PlayerTurn).with_system(player_input));
    }
}

pub fn try_move_player(
    delta_x: i32,
    delta_y: i32,
    commands: &mut Commands,
    query: &mut Query<(Entity, &mut Position, &mut Viewshed), With<Player>>,
    target_query: Query<(Entity, &CombatStats), Without<Player>>,
    map: &Res<Map>,
) {
    for (entity, mut pos, mut viewsheds) in query.iter_mut() {
        let destination_idx = map.xy_idx(pos.x + delta_x, pos.y + delta_y);
        // are there any potential targets
        for potential_target in map.tile_content[destination_idx].iter() {
            if let Ok((target, _)) = target_query.get(*potential_target) {
                commands.entity(entity).insert(WantsToMelee { target });
                return;
            }
        }

        if !map.blocked[destination_idx] {
            pos.x = min(79, max(0, pos.x + delta_x));
            pos.y = min(49, max(0, pos.y + delta_y));

            viewsheds.dirty = true;
        }
    }
}

pub fn player_input(
    mut commands: Commands,
    mut query: Query<(Entity, &mut Position, &mut Viewshed), With<Player>>,
    target_query: Query<(Entity, &CombatStats), Without<Player>>,
    map: Res<Map>,
    mut input: ResMut<Input<KeyCode>>,
    mut state: ResMut<State<RunState>>,
) {
    let mut movement = (0, 0);
    if input.any_just_pressed([KeyCode::Left, KeyCode::Numpad4, KeyCode::H]) {
        movement = (-1, 0);
    }
    if input.any_just_pressed([KeyCode::Right, KeyCode::Numpad6, KeyCode::L]) {
        movement = (1, 0);
    }
    if input.any_just_pressed([KeyCode::Down, KeyCode::Numpad2, KeyCode::K]) {
        movement = (0, 1);
    }
    if input.any_just_pressed([KeyCode::Up, KeyCode::Numpad8, KeyCode::J]) {
        movement = (0, -1);
    }

    if input.any_just_pressed([KeyCode::Numpad9, KeyCode::U]) {
        movement = (1, -1);
    }
    if input.any_just_pressed([KeyCode::Numpad7, KeyCode::Y]) {
        movement = (-1, -1);
    }
    if input.any_just_pressed([KeyCode::Numpad3, KeyCode::N]) {
        movement = (1, 1);
    }
    if input.any_just_pressed([KeyCode::Numpad1, KeyCode::B]) {
        movement = (-1, 1);
    }

    match movement {
        (x, y) if x == 0 && y == 0 => (),
        _ => {
            try_move_player(
                movement.0,
                movement.1,
                &mut commands,
                &mut query,
                target_query,
                &map,
            );
            if let Err(error) = state.set(RunState::MonsterTurn) {
                error!("oh noes! set state borked!\n{:?}", error);
            }
        }
    }
    // clear input
    input.clear();
}
