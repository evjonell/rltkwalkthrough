use crate::{BlocksTile, Map, Position};
use bevy::prelude::*;

pub struct MapIndexingSystem;
impl Plugin for MapIndexingSystem {
    fn build(&self, app: &mut App) {
        app.add_system(process_map);
    }
}

fn process_map(
    // mut commands: Commands,
    query: Query<(Entity, &Position)>,
    blockers: Query<&BlocksTile>,
    mut map: ResMut<Map>,
) {
    // let (mut map, positions, blockers, entities) = data;

    map.populate_blocked();
    map.clear_content_index();
    for (entity, position) in query.iter() {
        let idx = map.xy_idx(position.x, position.y);
        // if they block, update the block list
        if let Ok(_may_block) = blockers.get(entity) {
            map.blocked[idx] = true;
        }

        // push entity to the appropriate index. this is a copy
        // type, so we dont need to clone it (we dont want to move it)
        map.tile_content[idx].push(entity);
    }
}
