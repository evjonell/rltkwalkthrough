use bevy::prelude::*;
use bracket_bevy::BracketContext;

use crate::gui::draw_ui;

pub struct GameLog {
    pub entries: Vec<String>,
}

pub struct GameLogPlugin;

impl Plugin for GameLogPlugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system(game_log_setup)
            .add_system(draw_messages.after(draw_ui));
    }
}

fn game_log_setup(mut commands: Commands) {
    commands.insert_resource(GameLog {
        entries: vec!["Welcome!".to_string()],
    });
}

fn draw_messages(ctx: ResMut<BracketContext>, log: Res<GameLog>) {
    let mut y = 44;
    for entry in log.entries.iter().rev() {
        if y < 49 {
            ctx.print(2, y, entry);
        }
        y += 1;
    }
}
