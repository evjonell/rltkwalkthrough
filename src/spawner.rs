use bevy::prelude::*;
use bracket_bevy::{prelude::*, FontCharType};

use crate::{
    components::{BlocksTile, CombatStats, Monster, Player, Position, Renderable, Viewshed},
    map::MAPWIDTH,
    rect::Rect,
};

pub const MAX_MONSTERS: i32 = 4;
pub const MAX_ITEMS: i32 = 2;

pub fn spawn_player(commands: &mut Commands, player_x: i32, player_y: i32) -> Entity {
    commands
        .spawn()
        .insert(Position {
            x: player_x,
            y: player_y,
        })
        .insert(Renderable {
            glyph: to_cp437('@'),
            fg: RGB::named(YELLOW),
            bg: RGB::named(BLACK),
        })
        .insert(Player {})
        .insert(Viewshed {
            visible_tiles: Vec::new(),
            range: 8,
            dirty: true,
        })
        .insert(Name::new("Player"))
        .insert(CombatStats {
            max_hp: 30,
            hp: 30,
            defense: 1,
            power: 5,
        })
        .id()
}

pub fn spawn_monster<S: ToString>(
    commands: &mut Commands,
    x: i32,
    y: i32,
    glyph: FontCharType,
    name: S,
) -> Entity {
    commands
        .spawn()
        .insert(Position { x, y })
        .insert(Renderable {
            glyph,
            fg: RGB::named(RED),
            bg: RGB::named(BLACK),
        })
        .insert(Viewshed {
            visible_tiles: Vec::new(),
            range: 8,
            dirty: true,
        })
        .insert(Monster {})
        .insert(Name::new(name.to_string()))
        .insert(BlocksTile {})
        .insert(CombatStats {
            max_hp: 16,
            hp: 16,
            defense: 1,
            power: 2,
        })
        .id()
}

fn orc(commands: &mut Commands, x: i32, y: i32) -> Entity {
    spawn_monster(commands, x, y, to_cp437('o'), "Orc")
}

fn goblin(commands: &mut Commands, x: i32, y: i32) -> Entity {
    spawn_monster(commands, x, y, to_cp437('g'), "Goblin")
}

pub fn spawn_random_monster(
    commands: &mut Commands,
    rng: &RandomNumbers,
    x: i32,
    y: i32,
) -> Entity {
    match rng.roll_dice(1, 2) {
        1 => orc(commands, x, y),
        _ => goblin(commands, x, y),
    }
}

pub fn spawn_room(commands: &mut Commands, rng: &RandomNumbers, room: &Rect) {
    let mut monster_spawn_points: Vec<usize> = Vec::new();

    let num_monsters = rng.roll_dice(1, MAX_MONSTERS + 2) - 3;
    for _i in 0..num_monsters {
        let mut added = false;
        while !added {
            let x = (room.x1 + rng.roll_dice(1, i32::abs(room.x2 - room.x1))) as usize;
            let y = (room.y1 + rng.roll_dice(1, i32::abs(room.y2 - room.y1))) as usize;
            let idx = (y * MAPWIDTH) + x;
            if !monster_spawn_points.contains(&idx) {
                monster_spawn_points.push(idx);
                added = true;
            }
        }
    }

    // Actually spawn the monsters
    for idx in monster_spawn_points.iter() {
        let x = *idx % MAPWIDTH;
        let y = *idx / MAPWIDTH;
        spawn_random_monster(commands, rng, x as i32, y as i32);
    }
}
