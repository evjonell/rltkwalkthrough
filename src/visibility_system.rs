use bevy::prelude::*;
use bracket_bevy::prelude::*;
use bracket_pathfinding::prelude::*;

use super::{Map, Player, Position, Viewshed};

pub struct VisibilityPlugin;

impl Plugin for VisibilityPlugin {
    fn build(&self, app: &mut App) {
        app.add_system(process_visibility);
    }
}

fn process_visibility(
    // mut commands: Commands,
    mut query: Query<(Entity, &mut Viewshed, &Position)>,
    player_query: Query<&Player>,
    mut map: ResMut<Map>,
) {
    // let (mut map, entities, mut viewshed, pos, player) = data;
    for (entity, mut viewshed, pos) in query.iter_mut() {
        if viewshed.dirty {
            viewshed.dirty = false;
            // viewshed.visible_tiles.clear();
            viewshed.visible_tiles = field_of_view(Point::new(pos.x, pos.y), viewshed.range, &*map);
            viewshed
                .visible_tiles
                .retain(|p| p.x >= 0 && p.x < map.width && p.y >= 0 && p.y < map.height);

            // update player vis
            if let Ok(_player) = player_query.get(entity) {
                for t in map.visible_tiles.iter_mut() {
                    *t = false;
                }
                for vis in viewshed.visible_tiles.iter() {
                    let idx = map.xy_idx(vis.x, vis.y);
                    map.revealed_tiles[idx] = true;
                    map.visible_tiles[idx] = true;
                }
            }
        }
    }
}
