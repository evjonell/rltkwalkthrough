use bevy::prelude::*;
use bracket_bevy::prelude::*;
use bracket_pathfinding::prelude::*;

use crate::{
    components::{Player, WantsToMelee},
    Map, Monster, Position, RunState, Viewshed,
};

pub struct MonsterPlugin;

impl Plugin for MonsterPlugin {
    fn build(&self, app: &mut App) {
        app.add_system_set(
            SystemSet::on_update(RunState::MonsterTurn).with_system(process_monster_ai),
        );
    }
}

fn process_monster_ai(
    mut commands: Commands,
    player_query: Query<(Entity, &Position), With<Player>>,
    mut monster_query: Query<
        (Entity, &mut Viewshed, &mut Position),
        (With<Monster>, Without<Player>),
    >,
    mut map: ResMut<Map>,
    mut state: ResMut<State<RunState>>,
) {
    if let Ok((player, player_pos)) = player_query.get_single() {
        for (monster, mut viewshed, mut monster_pos) in monster_query.iter_mut() {
            let distance = DistanceAlg::Pythagoras.distance2d(
                Point::new(monster_pos.x, monster_pos.y),
                Point::new(player_pos.x, player_pos.y),
            );
            if distance < 1.5 {
                commands
                    .entity(monster)
                    .insert(WantsToMelee { target: player });
            } else if viewshed
                .visible_tiles
                .contains(&Point::new(player_pos.x, player_pos.y))
            {
                let path = a_star_search(
                    map.xy_idx(monster_pos.x, monster_pos.y) as i32,
                    map.xy_idx(player_pos.x, player_pos.y) as i32,
                    &*map,
                );
                if path.success && path.steps.len() > 1 {
                    // // we take the 2nd point, since the first is the monster position
                    // if let Some(step) = path.steps.get(1) {
                    let mut idx = map.xy_idx(monster_pos.x, monster_pos.y);
                    map.blocked[idx] = false;
                    monster_pos.x = path.steps[1] as i32 % map.width;
                    monster_pos.y = path.steps[1] as i32 / map.width;
                    idx = map.xy_idx(monster_pos.x, monster_pos.y);
                    map.blocked[idx] = true;
                    viewshed.dirty = true;
                    // }
                }
            }
        }
    }

    if state.current() == &RunState::MonsterTurn {
        if let Err(error) = state.set(RunState::Running) {
            error!("could not set state from monster ai!\n{:?}", error);
        }
    }
}
