use bevy::prelude::*;

use crate::{
    components::{CombatStats, Player, SufferDamage},
    gamelog::GameLog,
};

pub struct DamagePlugin;

impl Plugin for DamagePlugin {
    fn build(&self, app: &mut App) {
        app.add_system(apply_damage).add_system(remove_dead);
    }
}

fn apply_damage(
    mut commands: Commands,
    mut dmg_query: Query<(Entity, &mut CombatStats, &SufferDamage)>,
) {
    for (entity, mut stats, suffer_dmg) in dmg_query.iter_mut() {
        stats.hp -= suffer_dmg.amount.iter().sum::<i32>();
        commands.entity(entity).remove::<SufferDamage>();
    }
}

fn remove_dead(
    mut commands: Commands,
    query: Query<(Entity, &Name, &CombatStats)>,
    player_query: Query<&Player>,
    mut game_log: ResMut<GameLog>,
) {
    for (entity, name, stats) in query.iter() {
        if stats.hp <= 0 {
            if let Ok(_player) = player_query.get(entity) {
                game_log.entries.push("You died.".to_string());
                commands.entity(entity).despawn();
            } else {
                game_log.entries.push(format!("{} dies.", name));
                commands.entity(entity).despawn();
            }
        }
    }
}
