use bevy::prelude::*;

use crate::{
    components::{CombatStats, SufferDamage, WantsToMelee},
    gamelog::GameLog,
};

pub struct CombatPlugin;

impl Plugin for CombatPlugin {
    fn build(&self, app: &mut App) {
        app.add_system(melee_combat_system);
    }
}

fn melee_combat_system(
    mut commands: Commands,
    attacker_query: Query<(Entity, &WantsToMelee, &CombatStats, &Name)>,
    target_query: Query<(Entity, &CombatStats, &Name)>,
    mut dmg_query: Query<&mut SufferDamage>,
    mut game_log: ResMut<GameLog>,
) {
    for (attacker, wants_to_melee, stats, name) in attacker_query.iter() {
        if stats.hp > 0 {
            if let Ok((target, target_stats, target_name)) = target_query.get(wants_to_melee.target)
            {
                if target_stats.hp > 0 {
                    let dmg = i32::max(0, stats.power - target_stats.defense);
                    if dmg == 0 {
                        // info!("{} is unable to hurt {}", name, target_name);
                        game_log
                            .entries
                            .push(format!("{} is unable to hurt {}", name, target_name));
                    } else {
                        game_log
                            .entries
                            .push(format!("{} hits {} for {} damage", name, target_name, dmg));
                        SufferDamage::new_damage(&mut commands, &mut dmg_query, target, dmg);
                    }
                }
            }
        }
        commands.entity(attacker).remove::<WantsToMelee>();
    }
}
