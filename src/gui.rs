use bevy::prelude::*;
use bracket_bevy::{
    prelude::Point,
    BracketContext,
};

use crate::{
    components::{CombatStats, Player, Position},
    main_tick,
    map::Map,
};

pub struct GuiPlugin;

impl Plugin for GuiPlugin {
    fn build(&self, app: &mut App) {
        app.add_system(draw_ui.after(main_tick))
            .add_system(draw_tooltips.after(draw_ui));
    }
}

pub fn draw_ui(
    mut _commands: Commands,
    player_query: Query<&CombatStats, With<Player>>,
    ctx: ResMut<BracketContext>,
) {
    ctx.draw_box(0, 43, 79, 6, Color::WHITE, Color::BLACK);

    if let Ok(stats) = player_query.get_single() {
        let health = format!("HP: {} / {}", stats.hp, stats.max_hp);
        ctx.print_color(12, 43, health, Color::YELLOW, Color::BLACK);
        ctx.draw_bar_horizontal(28, 43, 51, stats.hp, stats.max_hp, Color::RED, Color::BLACK);
    }
    // let mouse = ctx.get_mouse_position_for_current_layer();
    // ctx.set(
    //     mouse.x,
    //     mouse.y,
    //     Color::rgba(1., 0., 1., 0.5),
    //     Color::rgba(1., 0., 1., 0.5),
    //     0,
    // );
}

fn draw_tooltips(query: Query<(&Name, &Position)>, ctx: ResMut<BracketContext>, map: Res<Map>) {
    let mouse_pos = ctx.get_mouse_position_for_current_layer();
    if mouse_pos.x >= map.width || mouse_pos.y >= map.height {
        return;
    }
    let mut tooltip = Vec::<&Name>::new();
    for (name, position) in query.iter() {
        let idx = map.xy_idx(position.x, position.y);
        if position.x == mouse_pos.x && position.y == mouse_pos.y && map.visible_tiles[idx] {
            tooltip.push(name);
        }
    }
    if !tooltip.is_empty() {
        let mut width: i32 = 0;
        for s in tooltip.iter() {
            if width < s.len() as i32 {
                width = s.len() as i32;
            }
        }
        width += 3;
        if mouse_pos.x > 40 {
            let arrow_pos = Point::new(mouse_pos.x - 2, mouse_pos.y);
            let left_x = mouse_pos.x - width;
            let mut y = mouse_pos.y;
            for s in tooltip.iter() {
                ctx.print_color(left_x, y, s, Color::WHITE, Color::GRAY);
                let padding = (width - s.len() as i32) - 1;
                for i in 0..padding {
                    ctx.print_color(
                        arrow_pos.x - i,
                        y,
                        &" ".to_string(),
                        Color::WHITE,
                        Color::GRAY,
                    );
                }
                y += 1;
            }
            ctx.print_color(
                arrow_pos.x,
                arrow_pos.y,
                &"->".to_string(),
                Color::WHITE,
                Color::GRAY,
            );
        } else {
            let arrow_pos = Point::new(mouse_pos.x + 1, mouse_pos.y);
            let left_x = mouse_pos.x + 3;
            let mut y = mouse_pos.y;
            for s in tooltip.iter() {
                ctx.print_color(left_x + 1, y, s, Color::WHITE, Color::GRAY);
                let padding = (width - s.len() as i32) - 1;
                for i in 0..padding {
                    ctx.print_color(
                        arrow_pos.x + 1 + i,
                        y,
                        &" ".to_string(),
                        Color::WHITE,
                        Color::GRAY,
                    );
                }
                y += 1;
            }

            ctx.print_color(
                arrow_pos.x,
                arrow_pos.y,
                &"<-".to_string(),
                Color::WHITE,
                Color::GRAY,
            );
        }
    }
}
