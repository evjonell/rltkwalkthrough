use bevy::{app::AppExit, prelude::*};
use bracket_bevy::prelude::*;
use combat::CombatPlugin;
use damage::DamagePlugin;
use gamelog::GameLogPlugin;
use gui::GuiPlugin;
use map_indexing_system::MapIndexingSystem;
use monster_ai_system::MonsterPlugin;
use player::PlayerPlugin;
use spawner::{spawn_player, spawn_room};
use visibility_system::VisibilityPlugin;

mod combat;
mod components;
mod damage;
mod gamelog;
mod gui;
mod map;
mod map_indexing_system;
mod monster_ai_system;
mod player;
mod rect;
mod spawner;
mod visibility_system;

use crate::{components::*, map::*, rect::Rect};

#[derive(PartialEq, Copy, Clone, Hash, Debug, Eq)]
pub enum RunState {
    Start,
    PlayerTurn,
    MonsterTurn,
    Running,
}

fn main() {
    let bterm = BTermBuilder::simple_80x50()
        .with_named_color("blue", BLUE)
        .with_named_color("pink", PINK)
        .with_random_number_generator(true);

    App::new()
        .add_plugins(DefaultPlugins)
        .add_plugin(bterm)
        .add_state(RunState::Running)
        .add_startup_system(setup_game)
        .add_system(main_tick)
        .add_system(check_for_exit)
        .add_plugin(GameLogPlugin)
        .add_plugin(PlayerPlugin)
        .add_plugin(VisibilityPlugin)
        .add_plugin(MapIndexingSystem)
        .add_plugin(MonsterPlugin)
        .add_plugin(CombatPlugin)
        .add_plugin(DamagePlugin)
        .add_plugin(GuiPlugin)
        .add_system_set(SystemSet::on_update(RunState::Running).with_system(upkeep))
        .run();
}

fn upkeep(mut state: ResMut<State<RunState>>) {
    // do any upkeep, then start player turn

    if let Err(error) = state.set(RunState::PlayerTurn) {
        error!("could not set state from upkeep!\n{:?}", error);
    }
}

pub fn main_tick(
    _commands: Commands,
    query: Query<(&Position, &Renderable)>,
    mut ctx: ResMut<BracketContext>,
    map: Res<Map>,
) {
    ctx.cls();

    draw_map(&mut ctx, &map);

    for (pos, render) in query.iter() {
        let idx = map.xy_idx(pos.x, pos.y);
        if map.visible_tiles[idx] {
            ctx.set(pos.x, pos.y, render.fg, render.bg, render.glyph);
        }
    }
}

fn setup_game(mut commands: Commands, rng: Res<RandomNumbers>) {
    let map: Map = Map::new_map_rooms_and_corridors(&rng);
    let (player_x, player_y) = map.rooms[0].center();

    spawn_player(&mut commands, player_x, player_y);

    for room in map.rooms.iter().skip(1) {
        spawn_room(&mut commands, &rng, room);
        // let (x, y) = room.center();
        // spawn_random_monster(&mut commands, &rng, x, y);
    }

    commands.insert_resource(map);
}

fn check_for_exit(keyboard: Res<Input<KeyCode>>, mut events: EventWriter<AppExit>) {
    if keyboard.pressed(KeyCode::Escape) {
        events.send(AppExit);
    }
}
